<?php // -->

// manual autoload
spl_autoload_register(function($class) {
    $class = str_replace('\\', '/', $class);
    $class = str_replace('GoIP/', '', $class);
    $class = dirname(__DIR__) . '/goip/src/' . $class . '.php';
    require $class;
});

// require server class
require dirname(__DIR__) . '/goip/src/Server.php';
require_once('functions.php');
// initialize server
// - hostname to bind to
// - port to bind to
    
//$server = new GoIP\Server('159.89.140.19', 8080);
//$server = new GoIP\Server('64.225.115.177', 80);
$server = new GoIP\Server('142.93.177.69', 8080);

$server
// set timeout before reading next data
->setReadTimeout(1)

// on connection bind
->on('bind', function($server) {
    echo 'Socket binded to: ' . $server->getHost() . ':' . $server->getPort() . PHP_EOL . PHP_EOL;
})

// on request data
->on('data', function($server, $buffer) {
    echo 'Server got buffer data from: ' . $server->getOrigin('host') . ':' . $server->getOrigin('port') . PHP_EOL;
    echo GoIP\Util::parseString($buffer);
    echo PHP_EOL;
})

// on keep-alive request ack
->on('ack', function($server,$buffer) {
    echo 'Keep-Alive request acknowledged.' . PHP_EOL . PHP_EOL;
    $det = GoIP\Util::parseArray($buffer);
    update_chan_status($det['id'],$det['gsm_status'],$det['signal']);
    print_r($det);
    echo PHP_EOL;
})

// on ack failed
->on('ack-fail', function() {
    echo 'Keep-Alive request acknowledgement failed.' . PHP_EOL . PHP_EOL;
})

// on message receive
->on('message', function($server, $buffer) {
    echo "\033[32mServer got a message from: " . $server->getOrigin('host') . ":" . $server->getOrigin('port') . " \033[0m" . PHP_EOL;
    echo "\033[32m" . GoIP\Util::parseString($buffer) . " \033[0m" ;
    $det = GoIP\Util::parseArray($buffer);
    print_r($det);
    echo "Message received clear";
    var_dump(strpos($det['msg'],"rent balance"));
    $l = db();
    $sql = "insert into all_messages set goip_chan_id='".$det['id']."',msg='".$det['msg']."'";
    mysqli_query($l,$sql);
    if(strpos($det['msg'],"rent balance"))
    {
       echo "Processing Current balance......................\n";
       reconcile($det['msg'],$det['id']);

    }
    if(strpos($det['msg'],"transferred"))
    {
        echo "Processing Current SAMBAZA......................\n";
        update_sambaza($det['id'],$det['msg']);
    }
    if($det['id'] == "kopokopo")
    {
        echo "Processing goip kopokopo msg......................\n";
        update_goip($det['id'],$det['msg']);
    }
    echo PHP_EOL;

    // $server->end();
})

// on wait (waiting for valid data)
->on('wait', function($server) {
    echo 'Waiting for the client to send data.' . PHP_EOL . PHP_EOL;
})

// on server end
->on('end', function() {
    echo 'Server got exit event. Terminating.' . PHP_EOL . PHP_EOL;
})

// start receiving connection data
->loop();










