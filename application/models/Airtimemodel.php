
<?php

class Airtimemodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

        function new_recon_tx($id,$line,$tx_status)
	{
	  $data['goip_chan_id']=$line;
	  $data['tx_id']=$id;
	  $data['tx_status'] = $tx_status;
	  $this->db->set('tx_start','now()',false);
	  $this->db->insert('reconcile',$data);

	}
	function new_api_tx($amount,$network,$src,$num_to_pay,$num_top_up,$ref)
	{
	  $data = array();
	  $data['amount'] = $amount; 
	  $data['network'] = $network;
	  $data['num_to_top_up'] = $num_top_up;
	  $data['num_paying'] = $num_to_pay;
	  $data['topup_ref'] = $ref;
	  $data['tx_src'] = $src;
	  $this->db->set('api_init_time','now()',false);
	  $this->db->set('stk_init_time','now()',false);
	  $this->db->insert('api_transactions',$data);


	}
	function update_stk_init_resp($checkoutRequestID,$merchantRequestID,$responseCode,$ref)
	{
           $sql = "update api_transactions set stk_end_time=now(),stk_init_resp='$responseCode',stk_merchant_request_id='$merchantRequestID',
	   stk_checkout_request_id='$checkoutRequestID' where topup_ref='$ref'";
	   $this->db->query($sql);


	}
	function check_topup_tx($topup_ref)
	{
           $data = array('topup_ref'=>$topup_ref);
	   $this->db->where($data);
	    $query = $this->db->get('api_transactions');
	    $ret = $query->row();
	    //error_log(print_r((array)$ret,true));
            $mpesa_ref = $ret->mpesa_ref;
	    
	    //error_log($this->db->last_query()." -->$status");
	    if(strlen($mpesa_ref) > 0)
	    {
	       return $mpesa_ref;

	    }
	   return false;


	}
	function check_stk_tx($checkoutRequestID)
	{
           $data = array('stk_checkout_request_id'=>$checkoutRequestID);
	   $this->db->where($data);
	    $query = $this->db->get('api_transactions');
	    $ret = $query->row();
	    //error_log(print_r((array)$ret,true));
            $status = $ret->stk_final_resp;
	    
	    //error_log($this->db->last_query()." -->$status");
	    if(strlen($status) > 0 || $ret->stk_final_resp_desc == 'DS timeout.')
	    {
               $resp = array();
	       
	       $resp['status'] = $ret->stk_final_resp;
	       $resp['desc'] =  $ret->stk_final_resp_desc;
	       $resp['mpesa_ref'] =  $ret->mpesa_ref ;
	       $resp['mpesa_date'] =  $ret->mpesa_date ;
	       return $resp;

	    }
	   return false;


	}
	function update_stk_final_resp($checkoutRequestID,$status,$desc,$mpesa_ref,$mpesa_date)
	{
          $sql = "update api_transactions set stk_final_resp_desc='$desc',mpesa_ref='$mpesa_ref',mpesa_date='$mpesa_date', stk_final_time=now(),stk_final_resp='$status' where stk_checkout_request_id='$checkoutRequestID'";
	   $this->db->query($sql);
	  

	}
   	function check_goip_resp($id)
   	{

           $data = array('id'=>$id ,'tx_status'=>'SUCCESS');
	   $this->db->where($data);
	   $num = $this->db->count_all_results('sambaza_det');
	   if($num > 0)
	   {
	     return true;
	   }
	   return false;



   	}
	function get_graph_data()
	{
         $sql = " select sum(amount) as amt, date(tx_date) as tx from mpesa_details group by tx order by tx desc limit 10";
	 $query = $this->db->query($sql);
	 $all = array();
	 $det = array('Date','Amount');
	 //$all[] = $det;
	 $temp = array();
	 foreach ($query->result() as $row)
	 {
	    $res = array(str_replace("2020-","",$row->tx),(int)$row->amt);
	    $all[] = $res;
	     
	 }
	 $all = array_reverse($all);
	 array_unshift($all,$det);
	 $resp = json_encode($all);
	 //error_log($resp);
	 return $resp;

	}
	function get_balance($line)
	{
         $sql = "select bal_after from sambaza_det where chan_num='$line' order by id desc limit 1";
	 $res = $this->db->query($sql);
	 $r = $res->row();
	 return $r->bal_after;


	}
        function line_available($line_num)
        {
	    $this->db->where('chan_num', $line_num);
	    $query = $this->db->get('channels');
	    $ret = $query->row();
            $status = $ret->chan_gsm_status;
	    $last_time = strtotime($ret->chan_status_updated_date);
	    $signal = $ret->chan_signal;
	    //rssi signal not 99, logged in and last update in last 60 secs->pptp is up
	    if($signal < 99 && $status == "LOGIN" && (time() - $last_time <= 60))
	    {
	      return true;
	    }
	    return false;
		  
        }
	function new_sambaza($chan_num,$phone,$amount,$mpesa_ref)
	{
	  $sql = "insert into sambaza_det set chan_num='$chan_num',phone='$phone',amount='$amount',mpesa_ref='$mpesa_ref',tx_start=now(),tx_status='SENT TO GOIP'";
	  $this->db->query($sql);
	  $insert_id = $this->db->insert_id();
	  return  $insert_id;

	}
	function update_failed_sambaza($id,$bal)
	{
           $sql = "update sambaza_det set bal_after='$bal', tx_status='GOIP NOT REACHABLE',tx_end=NOW() where id='$id'";
	   $this->db->query($sql);
	}
	function update_sambaza($id,$code)
	{
           $sql = "update sambaza_det set http_resp_code='$code' where id='$id'";
	   $this->db->query($sql);
	}
	function save_kopokopo_data($data)
	{
	   $this->db->where('mpesa_ref', $data['mpesa_ref']);
	   $num = $this->db->count_all_results('mpesa_details');
	   //var_dump($num);
	   if($num)// mpesa code exists so exit
	   {
	//	 echo "wtf";
		 return 202;   
		   
	   }
	   elseif($this->db->insert('mpesa_details', $data))
	   {
            return true;
	   }
	   else	
	   {
			return print_r($this->db()->error(),true);
	   }
	}
	function check_last_purchase($phone,$amount,$mpesa_ref)
	{
	   $this->db->where('phone', $phone);
	   $num = $this->db->count_all_results('purchases');
	   //var_dump($num);echo "last purchase";
	   if($num)
	   {
		   $this->db->where('phone', $phone);
		  $query = $this->db->get('purchases');
		  $ret = $query->row();
          $time = $ret->purchase_time ;
		  $earliest_time = $time + 360; //6 minutes later
		  $msg = "Kindly wait 5 minutes between airtime requests. Your airtime will be loaded at ". date('h:i:s A',$earliest_time);
		  $this->add_pending_purchase($phone,$earliest_time - 40,$amount,$mpesa_ref);
		  return msg;
	   }
	   return false;
		
	}
	
	function update_last_purchase_info()
	{
		$now = time();
		$last = $now - (6 * 60); //last 6 minutes
		//$array = array('purchase_time <= ' => $last);
        $this->db->where('purchase_time <= ', $last);         
        $this->db->delete('purchases'); 		
		
	}
	
	function add_pending_purchase($phone,$timestamp,$amount,$mpesa_ref)
	{
		$data['phone'] = $phone;
		$data['purchase_time'] = $timestamp;
		$data['amount'] = $amount;
		$data['mpesa_ref'] = $mpesa_ref."-".rand(1,99);
		if($this->db->insert('pending_purchases', $data))
		{
            return true;
		}
		else	
		{
			error_log($this->db()->error(),true);
			return print_r($this->db()->error(),true);
		}
		
		
	}
	function new_purchase($phone)
	{
		$data['phone'] = $phone;
		$data['purchase_time'] = time();		
		if($this->db->insert('purchases', $data))
		{
            return true;
		}
		else	
		{
			return print_r($this->db()->error(),true);
		}
				
	}
	
	function new_topup($data)
	{
		if($this->db->insert('topups', $data))
		{
            return true;
		}
		else	
		{
			return print_r($this->db()->error(),true);
		}
		
	}
	
	function update_topup($req_id,$status)
	{
		$sql = "update topups set date_updated=now(),final_status='$status' where request_id='$req_id'";
		$query = $this->db->query($sql);
		
	}
	function fetch_pending()
	{
		$query = $this->db->get('pending_purchases');
		$result = $query->result_array();
		
		return $result;


	}
    function delete_pending($id)
	{
		$this->db->where('id', $id);         
        $this->db->delete('pending_purchases');


	}
    function add_history_pending_purchase($id,$phone,$timestamp,$amount,$mpesa_ref)
	{
		$data['phone'] = $phone;
		$data['purchase_time'] = $timestamp;
		$data['amount'] = $amount;
		$data['mpesa_ref'] = $mpesa_ref;
		$data['id'] = $id;

		if($this->db->insert('hpending_purchases', $data))
		{
            return true;
		}
		else	
		{
			return print_r($this->db()->error(),true);
		}
		
		
	}	
	
}
