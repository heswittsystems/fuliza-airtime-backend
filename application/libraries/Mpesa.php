<?php

class Mpesa
{

    /**
     * Lipa na M-PESA password
  
   * */
     private $url_req = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
     private $stk_push_status_url = 'https://api.safaricom.co.ke/mpesa/stkpushquery/v1/query';
     private $cred_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
     private $passkey = "896c08ea6b24688cc9b57e78d3ef29e45b5a3dff7d172f286d2eaa9116bfaf83";
     private $short_code = 7927783;
     #private $callback_url = "https://167.71.82.55/airtime/index.php/Airtime/stkfinalresp";
     #private $callback_url = "https://rds.co.ke/airtime/index.php/Airtime/stkfinalresp";
     private $callback_url = "https://142.93.177.69/rds/airtime/index.php/Airtime/stkfinalresp";
     //private $callback_url = "https://142.93.177.69/test.php";
     #private $callback_url = "https://www.youthgreensociety.org/store/";
     private $consumer_key = "xBQgitnAFFSpil4nqg1z8LAx4X7ljDgM";
     private $consumer_secret = "ENud62HnefBOVeIm";

    public function lipaNaMpesaPassword()
    {
        $lipa_time = '20'.date("ymdhis");
        $passkey = $this->passkey;//"bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
        $BusinessShortCode = $this->short_code;//174379;
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);
	$res = array();
	$res[0] = $lipa_na_mpesa_password;
	$res[1] = $timestamp;
        return $res;
    }


    /**
     * Lipa na M-PESA STK Push method
     * */

    public function customerMpesaSTKPush($amount,$phone,$ref,$desc)
    {
        #$url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $url = $this->url_req;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generateAccessToken()));
	$passwd = $this->lipaNaMpesaPassword();
        $phone = "254".substr($phone,-9);
        $curl_post_data = [
            //Fill in the request parameters with valid values
            'BusinessShortCode' => $this->short_code,
            'Password' => $passwd[0],
            'Timestamp' => $passwd[1],//"20".date('ymdhis'),
            'TransactionType' => 'CustomerBuyGoodsOnline',
            'Amount' => $amount,
            'PartyA' => $phone, // replace this with your phone number
            'PartyB' => "5621869",//$this->short_code,
            'PhoneNumber' => $phone, // replace this with your phone number
            'CallBackURL' => $this->callback_url,
            'AccountReference' => $ref,
            'TransactionDesc' => $ref
        ];

        $data_string = json_encode($curl_post_data,JSON_UNESCAPED_SLASHES);
	//var_dump($data_string);
	error_log($data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        error_log("starttime: ".time());
        $curl_response = curl_exec($curl);
        error_log("end time: ".time());

        return $curl_response;
    }


    public function generateAccessToken()
    {
        $consumer_key = $this->consumer_key;//"sMpgnYW62glBlxPXbyTBEGdPib8eJLOL";
        $consumer_secret = $this->consumer_secret;
        $credentials = base64_encode($consumer_key.":".$consumer_secret);

        $url = $this->cred_url;//"https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
        #$url = "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$credentials));
        curl_setopt($curl, CURLOPT_HEADER,false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        $access_token=json_decode($curl_response);
//	var_dump($access_token);
        return $access_token->access_token;
    }
    public function STKPushQuery($checkoutRequestID)
    {
       $url = $this->stk_push_status_url;

       $curl = curl_init();
       curl_setopt($curl, CURLOPT_URL, $url);
       curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generateAccessToken()));

        $passwd = $this->lipaNaMpesaPassword();
        $curl_post_data = array(
            'BusinessShortCode' => $this->short_code,
            'Password' => $passwd[0],
            'Timestamp' => $passwd[1],
            'CheckoutRequestID' => $checkoutRequestID
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HEADER, false);

        $curl_response = curl_exec($curl);

        return $curl_response;
    }


}    
?>
