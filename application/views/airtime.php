<?php
ini_set('max_execution_time', 0);
$this->load->helper('url');

?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Reports</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/datatables.min.css"/>
  <style>
  
  /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>


 

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark static-top bg-primary">
    <div class="container">
      <a class="navbar-brand" href="#">Heswitt Systems:</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/airtime/reps/4">Sales Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/airtime/reps/5">Mpesa Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/airtime/reps/6">Send SMS
              <span class="sr-only">(current)</span>
            </a>
          </li>
		  <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>index.php/airtime/graphs" target="_blank">Graph Report
              <span class="sr-only">(current)</span>
            </a>
          </li>
		 

         
        </ul>
      </div>
    </div>
  </nav>
<?php
function my_db()
{
	$li = mysqli_connect("localhost","root","dustbin");
	mysqli_select_db($li,"airtime");
	return $li;
	
}
function send_sms($num,$msg)
{
                //var_dump($msg);
        error_log("Sending message $msg to $num");
        $num = substr($num,1,strlen($num));
                //var_dump($num);
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://app.bongasms.co.ke/api/send-sms-v1",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => array('apiClientID' => '84','key' => 'ITWxgswUg3aRNrQ','secret' => '13tcYjGNCJHPsAGg6QpHg9FzgMRlM9','txtMessage' => $msg,'MSISDN' => $num,'serviceID' => '1')
                ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($curl);
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
                        var_dump($error_msg);
        }
                curl_close($curl);
                error_log($response);

}

extract($_GET);
echo "<p>$bal</p>";
if(@!$id)
{
	$id = "4";
}

if($id == "5")
{
	
	
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Mpesa Report</h1>
	  
	  <form method="post" action="index.php/Airtime/reps">
	  </div>
	  <div class="col-lg-4">
       <div class="form-group">
 <label ><b>Date From</b></label>
 <input type="date" name="from" max="3000-12-31" 
        min="1000-01-01" class="form-control">
</div>
</div>
<div class="col-lg-4">
<div class="form-group">
 <label ><b>Date To</b></label>
 <input type="date" name="to" min="1000-01-01"
        max="3000-12-31" class="form-control" >
</div>
</div>
<div class="col-lg-4">
<label ><b>&nbsp;</b></label>
<input type="submit" class="form-control btn btn-primary btn-lg" value="View Report" name="view">
</div>
</div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    
  </div>
<!-- End page content -->
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example3" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    <th>Phone</th>
                <th>Amount</th>
                <th>Names</th>
                <th>Tx Date</th>
				<th>Ref</th>
				
                
                
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = my_db();
		@$sql = "SELECT * from mpesa_details ";//where tx_date between '$from' and '$to' order by tx_date desc ";
		$re = mysqli_query($l,$sql);		
		$total = 0;
		$sales = 0;
		while($re2 = mysqli_fetch_array($re))
		{		
	      
			
			$total += $re2['amount'];
			//$sales += $revenue[1];
			echo "<tr>";			
			echo "<td>".$re2['phone']."</td>";
			echo "<td>".$re2['amount']."</td>";

		    echo "<td>".$re2['f_name']." ".$re2['m_name']." ".$re2['l_name']." </td>";
			echo "<td>".$re2['tx_date']."</td>";
			echo "<td>".$re2['mpesa_ref']."</td>";


            
			
			echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Phone</th>
                <th>Amount</th>
                <th>Names</th>
                <th>Tx Date</th>
				<th>Ref</th>
				
            </tr>
        </tfoot>
    </table>
	<h3>Total Mpesa:  <?php echo number_format($total,2);?></h3>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}
if($id == "6")
{
       if(@$_POST)
       {
          ini_set('max_execution_time', '0');
          ini_set("zlib.output_compression", 0);  // off
	  ini_set("implicit_flush", 1);  // on   
	  ob_implicit_flush(true);
          ob_end_flush();
          //print_r($_POST);
	  $msg = $_POST['msg'];
	  $mode = $_POST['mode'];
	  $l = my_db();
	  if($mode == "1")
	  {
	    $nums = get_nums();
	  }
	  else
	  {
	     $nums = array('+254715532259','+254711684717');
	  }
	  $n = 0;
	  $tot = count($nums);
          send_sms("+254715532259","Starting to send" );
	  echo "starting ".time()."</br>";;
	  while($n < count($nums))
	    //while($n < 5)
	    {
               
	       $phone = $nums[$n];
               error_log("Sending $n of $tot to $phone at ".time());
         
	       send_sms($phone,$msg);
	       //ob_flush();
	      // flush();
	       //sleep(1);
	       $n++;


	    }
          send_sms("+254715532259","finished to send" );
	  echo "ending ".time()."</br>";;
	    $qq = "insert into advert_sms set total_num='$n',sms='$msg'";
	     mysqli_query($l,$qq);
           ob_end_flush();

       }
	
	?>
	<!-- Page Content -->
  <div class="container">


    <div class="row">
      <div class="col-lg-12 text-center">
	  <h1 class="mt-5">Send SMS</h1>
	  
	  <form method="post" action="<?=base_url('index.php/airtime/reps/6')?>">
	  </div>
     <div class="col-lg-4">
      
       <label class="switch">
         <input type="checkbox" name="mode" value=1>
        <span class="slider"></span>
	</label><h3>Live Mode</h3>
    </div>
       <div class="col-lg-8">	
       <div class="form-group">
          <label ><b>SMS Message</b> <span id="count"></span></label>
          <textarea rows="4" cols="60" class="form-control" name="msg" required id="textarea"></textarea>
       </div>
     </div>
     <div class="col-lg-12">
        <input type="submit" class="form-control btn btn-success" value="Send SMS" name="sendmsg">
    </div>
   </div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    
  </div>
<!-- End page content -->
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example6" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    <th>Date</th>
                <th>Count</th>
                <th>SMS</th>
                
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = my_db();
		@$sql = "SELECT * from advert_sms order by id desc ";//where tx_date between '$from' and '$to' order by tx_date desc ";
		$re = mysqli_query($l,$sql);		
		$total = 0;
		$sales = 0;
		while($re2 = mysqli_fetch_array($re))
		{		
			echo "<tr>";			
			echo "<td>".$re2['tx_date']."</td>";
			echo "<td>".$re2['total_num']."</td>";
		        echo "<td>".$re2['sms']."</td>";
			echo "</tr>";
        }
        ?>		
        </tbody>
    </table>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	 <!-- Page Content -->
	
	<?php
	
}
if($id == "4")
{
	
	
	?>
	<!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <br>
	  <h1 class="mt-5">Sales Report</h1>
	  
	  <form method="post" action="index.php/Airtime/reps">
	  </div>
	  <div class="col-lg-4">
       <div class="form-group">
 <label ><b>Date From</b></label>
 <input type="date" name="from" max="3000-12-31" 
        min="1000-01-01" class="form-control">
</div>
</div>
<div class="col-lg-4">
<div class="form-group">
 <label ><b>Date To</b></label>
 <input type="date" name="to" min="1000-01-01"
        max="3000-12-31" class="form-control" >
</div>
</div>
<div class="col-lg-4">
<label ><b>&nbsp;</b></label>
<input type="submit" class="form-control btn btn-primary btn-lg" value="View Report" name="view">
</div>
</div>
	  </form>
	  <div class="clearfix"></div>
	  <br>
	  

     </div>
    
  </div>
<!-- End page content -->
	 <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    <th>Phone</th>
                <th>Amount</th>
                <th>Mpesa Ref</th>
                <th>Discount</th>
				<th>Init Date</th>
				<th>Resp Date</th>
				<th>Status</th>
                
                
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = my_db();
		@$sql = "SELECT * from topups ";//where tx_date between '$from' and '$to' order by tx_date desc ";
		$re = mysqli_query($l,$sql);		
		$total = 0;
		$sales = 0;
		while($re2 = mysqli_fetch_array($re))
		{		
	        $profits = explode("KES",$re2['discount']);
			$revenue = explode("KES",$re2['amount']);
			
			$total += $profits[1];
			$sales += $revenue[1];
			echo "<tr>";			
			echo "<td>".$re2['phone']."</td>";
			echo "<td>".$re2['amount']."</td>";

		    echo "<td>".$re2['mpesa_ref']."</td>";
			echo "<td>".$re2['discount']."</td>";
			echo "<td>".$re2['date_sent']."</td>";

		    echo "<td>".$re2['date_updated']."</td>";
			echo "<td>".$re2['final_status']."</td>";

            
			
			echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
                <th>Phone</th>
                <th>Amount</th>
                <th>Mpesa Ref.</th>
                <th>Discount</th>
				<th>Init. Date</th>
				<th>Resp. Date</th>
				<th>Status</th>
            </tr>
        </tfoot>
    </table>
	<h3>Total Sales: <?php echo number_format($sales,2);?> Total discount <?php echo number_format($total,2);?></h3>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	 <!-- Page Content -->
	  <br>
	  <center><h1 class="mt-5">Sambaza Sales Report</h1></center>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
	  <table id="example2" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
			    <th>Chan</th>
			    <th>Phone</th>
                <th>Amount</th>
                <th>Mpesa Ref</th>
                <th>Disc.</th>
		<th>Bal. After</th>
		<th>Init Date</th>
		<th>Resp Date</th>
		<th>Status</th>
                
                
            </tr>
        </thead>
        <tbody>
		<?php
		extract($_POST);
		$l = my_db();
		@$sql = "SELECT * from sambaza_det ";//where tx_date between '$from' and '$to' order by tx_date desc ";
		$re = mysqli_query($l,$sql);		
		$total = 0;
		$sales = 0;
		while($re2 = mysqli_fetch_array($re))
		{		
	        $profits = explode("KES",$re2['discount']);
			$revenue = explode("KES",$re2['amount']);
			
			$total += $re2['amount'] * 0.2;
			$sales += $re2['amount'];
			echo "<tr>";			
			echo "<td>".$re2['chan_num']."</td>";
			echo "<td>".$re2['phone']."</td>";
			echo "<td>".$re2['amount']."</td>";
		        echo "<td>".$re2['mpesa_ref']."</td>";
			echo "<td>".$re2['amount'] * 0.2."</td>";
			echo "<td>".$re2['bal_after']."</td>";
			echo "<td>".$re2['tx_start']."</td>";
			echo "<td>".$re2['tx_end']."</td>";
		    echo "<td>".$re2['tx_status']."</td>";
			echo "</tr>";
        }
        ?>		
        </tbody>
        <tfoot>
            <tr>
			    <th>Chan</th>
			    <th>Phone</th>
                <th>Amount</th>
                <th>Mpesa Ref</th>
                <th>Disc.</th>
		<th>Bal. After</th>
		<th>Init Date</th>
		<th>Resp Date</th>
		<th>Status</th>
                
            </tr>
        </tfoot>
    </table>
	<h3>Total Sales: <?php echo number_format($sales,2);?> Total discount <?php echo number_format($total,2);?></h3>
	  <?php
	if(@$_POST['view'])
	{
		//print_r($_POST);
	}
	?>
	   
     </div>
    </div>
  </div>
<!-- End page content -->
	
	<?php
	
}


?>
  
  
  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url();?>/jquery.js"></script>
  <script src="<?php echo base_url();?>/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/datatables.min.js"></script>

 <script type="text/javascript"> 
$(document).ready(function() {
    $('#example6').DataTable( {
    } );
} );
$(document).ready(function() {
    $('#example2').DataTable( {
        "order": [[ 6, "desc" ]]
    } );
} );

$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 4, "desc" ]]
    } );
} );
$(document).ready(function() {
    $('#example3').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
 
 
 function update(id)
 {
	 $.ajax({
            url : 'process.php?sid=' + id,
            type: "GET",
            data: $(this).serialize(),
            success: function (data) {
			   alert(data);
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
	 
	 
 }
 document.getElementById('textarea').onkeyup = function () {
   document.getElementById('count').innerHTML = " count: " + this.value.length;
   };

</script>
</body>

</html>
<?php

function get_nums()
{
	ini_set('display_errors',1);
//	require_once('functions.php');
	$l = my_db();
	$sql = "select distinct(phone) as phone from mpesa_details group by phone";
	$re = mysqli_query($l,$sql);
	echo mysqli_error($l);
	$n = 0;
	//echo mysqli_num_rows($re);
	//exit();
	$nums = array();
	while($re2 = mysqli_fetch_array($re))
	{
	  if(strlen($re2['phone']) > 8)
	  {
	     $nums[] = "+254".substr($re2['phone'],-9);
	  }
	}
	$sql = "select distinct(num_to_top_up) as phone from api_transactions;";
	$re = mysqli_query($l,$sql);
	echo mysqli_error($l);
	while($re2 = mysqli_fetch_array($re))
	{

	   $nums[] = "+254".substr($re2['phone'],-9);
	}

	$sql = "select distinct(num_paying) as phone from api_transactions;";
	$re = mysqli_query($l,$sql);
	echo mysqli_error($l);
	while($re2 = mysqli_fetch_array($re))
	{
	   $nums[] = "+254".substr($re2['phone'],-9);
	}
	//echo '<pre>';
	//print_r($nums);
	$unique = array_unique($nums);
	return $unique;
}


?>
