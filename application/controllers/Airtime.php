<?php
		header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Airtime extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() 
	{
		parent::__construct();

		// Load url helper
		$this->load->helper('url');
        $this->load->database();
        $this->load->model('airtimemodel');
	$this->load->library('mpesa');
		error_reporting(E_ALL ^ (E_NOTICE));
		ini_set('display_errors',0);
		header('Access-Control-Allow-Origin: *');
	
	        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
	    		    
	}
	function api()
        {
          $data = $this->input->post();
	  //echo file_get_contents("php://input");
          error_log(print_r($data,true));
         // echo print_r($data,true);
	  $this->new_api_tx($data['amount'],$data['network'],$data['src'],$data['num_pay'],$data['num_rec'],$data['tx_ref']);

        }
	function apistatus()
        {
          $data = $this->input->post();
	  //echo file_get_contents("php://input");
          error_log(print_r($data,true));
         // echo print_r($data,true);
             $paid = $this->airtimemodel->check_topup_tx($data['ref']);
	     if($paid === false)
	     {
	         $reply['code'] = "100";
		 $reply['desc'] = "Callback did not update";
	 	 
		 echo json_encode($reply);
		 return;
	     }
		 $reply['code'] = 200;
		 $reply['desc'] = "Success";
		 $reply['mpesa_ref'] = $paid;
		 echo json_encode($reply);
		 return;

        }

	function new_api_tx($amount,$network,$src,$num_to_pay,$num_top_up,$ref)
	{
	  $rep = $this->airtimemodel->new_api_tx($amount,$network,$src,$num_to_pay,$num_top_up,$ref);
	  if($amount > 500)
	  {
	         $reply['code'] = "100";
		 $reply['desc'] = "Maximum amount is 500";//$resp['errorMessage'];
	 	 ##fastcgi_finish_request();
		 if($amount > 2000 && ($num_to_pay != $num_top_up))
		 {
	            $phone = "+254".substr($num_top_up,1,strlen($num_top_up)); //check if it comes as 07...

		    $msg = "Stop using my business for your cons. GO FUCK YOURSELF!!!!";
                    //send_sms($phone,$msg);
		    $reply['desc'] = $msg;
                    
		 }
		 echo json_encode($reply);

		 return; 

	  }
	  $resp = $this->gen_stk_push($amount,$num_to_pay,$ref,"Topup");
	  if(count($resp) == 3)
	  {
	         $reply['code'] = "100";
		 $reply['desc'] = $resp['errorMessage'];
	 	 #fastcgi_finish_request();
		 echo json_encode($reply);

		 return; 

	  }

	  $rep = $this->airtimemodel->update_stk_init_resp($resp['CheckoutRequestID'],$resp['MerchantRequestID'],$resp['ResponseCode'],$ref);
//	  sleep(2);
	  error_log(print_r($resp,true));
	  $reply = array();
	  $paid = false;
	  $i = 0;
	  while(true)
	  {
             $paid = $this->airtimemodel->check_stk_tx($resp['CheckoutRequestID']);
	     //error_log("in loop");
	     if($paid === false)
	     {
	       sleep(3);
	       $i++;
	       error_log("checking at $i");
	       if($i == 29)//waited for 40 secs minute timeout is 60secs
	       {
	         $reply['code'] = "100";
		 $reply['desc'] = "Callback did not update";
	 	 #fastcgi_finish_request();
		 echo json_encode($reply);

		 return;
	       }
	     }
	     else
	     {
                break;
	     }

	  }
	   //$paid['status'] = "0";
	   //$paid['mpesa_ref'] = rand(999,99999)."GHS62";
	   if($paid['status'] == "0")//success
	   {
		    $det = array();
		    $det['amount'] = $amount;
		    $det['mpesa_ref'] = $paid['mpesa_ref'];
		    $det['tx_ref'] = $ref;
		    $det['mpesa_date'] = $paid['mpesa_ref'];
		    $det['num_rec'] = $num_top_up;
		    $reply['code'] = 200;
		    $reply['desc'] = "Success";
		    $reply['mpesa_ref'] = $paid['mpesa_ref'];
		    echo json_encode($reply);
		    #fastcgi_finish_request();
		    ob_start();
		    $re = $this->process($det);
		    ob_end_clean();
		    ob_start();
		    $output = ob_get_clean();
		    error_log("output is ". $output);
		    die($output);
		    //flush();
		    return;
	    }
	    else
	    {
               $reply['code'] = 300;
	       $reply['desc'] = $paid['desc'];

	       echo json_encode($reply);
	       return;



	    }



	}
	public function stkfinalresp()
	{
	  $data = file_get_contents("php://input");
	  error_log($data);
	  //$data = '{"Body":{  "stkCallback":{    "MerchantRequestID":"19465-780693-1",    "CheckoutRequestID":"ws_CO_130520201432346432",    "ResultCode":0,    "ResultDesc":"The service request is processed successfully.",    "CallbackMetadata":{      "Item":[{"Name":"Amount","Value":1},{"Name":"MpesaReceiptNumber","Value":"LGR7OWQX0R"},{"Name":"Balance"},{"Name":"TransactionDate","Value":20170727154800},{"Name":"PhoneNumber","Value":254721566839}      ]    }  }}	}';
          $det = json_decode($data,TRUE);
	  //error_log(print_r($det,true));
          //echo print_r($det,true);
	  $this->airtimemodel-> update_stk_final_resp($det['Body']['stkCallback']['CheckoutRequestID'],$det['Body']['stkCallback']['ResultCode'],$det['Body']['stkCallback']['ResultDesc'],$det['Body']['stkCallback']['CallbackMetadata']['Item'][1]['Value'],$det['Body']['stkCallback']['CallbackMetadata']['Item'][3]['Value']);


	}
	public function gen_stk_push($amount,$num_to_pay,$ref,$desc)
	{
	   $id1 = bin2hex(openssl_random_pseudo_bytes(2));
	   $id2 =  bin2hex(openssl_random_pseudo_bytes(2));
	   //echo strtoupper("$id1-$id2");
	   $res = $this->mpesa->customerMpesaSTKPush($amount,$num_to_pay,$ref,"test1");
//	   var_dump($res);
	   
	   $data = json_decode($res,TRUE);
	   /*sleep(2);
	   $rep = $this->mpesa->STKPushQuery($data['CheckoutRequestID']);
	   var_dump($rep); */
	   error_log($data);
	   return $data;

	}
	public function index()
	{
	  
		$this->load->view('welcome_message');
	}
	function get_kopokopo_payment_data() {
	        return $this->input->post();//this is for kopokopo. Rest is for postman
		$data = file_get_contents("php://input");
		error_log(print_r($data,true));
		//$det = $this->input->post();
		//error_log(print_r($det,true));
		return json_decode($data,TRUE);
		/* var_dump($data);
		$pairs = explode("&",$data );
		$vars = array();
		foreach ($pairs as $pair) {
			$nv = explode("=", $pair);
			$name = urldecode($nv[0]);
			$value = urldecode($nv[1]);
			$vars[$name] = $value;
		}
        return $vars; */
	}
	function err($msg,$level)
	{
		//lower the level the more serious
		
	}
	
    function send_sms($num,$msg)
	{
		//var_dump($msg);
		if($num == "+254715532259")
		{
                   $num = "+254711684717";  
		}
	error_log("Sending message $msg to $num");
        $num = substr($num,1,strlen($num));
		//var_dump($num);
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://app.bongasms.co.ke/api/send-sms-v1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('apiClientID' => '84','key' => 'ITWxgswUg3aRNrQ','secret' => '13tcYjGNCJHPsAGg6QpHg9FzgMRlM9','txtMessage' => $msg,'MSISDN' => $num,'serviceID' => '1')
		));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
		//	var_dump($error_msg);
        }
		curl_close($curl);
	//	echo ( $response);

	}
	function process($det = false)
    {
                error_log("lets start");
		//save payment data->check five minutes->buy->check if new
		if($det === false)
		{
			$postdata = $this->get_kopokopo_payment_data();
			error_log(print_r($postdata,true));
			//exit;
			extract($postdata);
			$data = array();
			$data['biz_number'] = $business_number;
			$data['amount'] = $amount;
			$data['mpesa_ref'] = $transaction_reference;
			$data['kopokopo_ref'] = $internal_transaction_id;
			$data['tx_time'] = $transaction_timestamp ;
			$data['phone'] = $sender_phone;
			$data['f_name'] = $first_name;
			$data['m_name'] = $middle_name;
			$data['l_name'] = $last_name;
			$data['final_status'] = 0;
			$data['final_status_date'] = 0;
		}
		else
		{
			$data = array();
			$data['biz_number'] = "5621869";
			$data['amount'] = $amount = $det['amount'] ;
			$data['mpesa_ref'] = $mpesa_ref = $transaction_reference = $det['mpesa_ref'];
			$data['kopokopo_ref'] = $internal_transaction_id =  $det['tx_ref'];;
			$data['tx_time'] =  $transaction_timestamp = $det['mpesa_date'] ;
			$data['phone'] = $sender_phone = $det['num_rec'];
			$data['f_name'] = "STK ";
			$data['m_name'] = " ";
			$data['l_name'] = " ";
			$data['final_status'] = 0;
			$data['final_status_date'] = 0;



		}
          	error_log(print_r($det,true));
          	error_log(print_r($data,true));
		$ret = $this->airtimemodel->save_kopokopo_data($data);

		//var_dump($ret);
		if($ret === 202)//duplicate mpesa code, raise alarm
		{
			error_log("duplicate mpesa code received"); 
			error_log("duplicate mpesa code received from phone $sender_phone and code $transaction_reference");			
			$this->send_sms("+254715532259","Duplicate mpesa code from phone $sender_phone and code $transaction_reference");
		    exit();
			
		}
		
		if($ret !== true)//mpesa data not saved
		{
			$this->err("save_kopokopo_data failed->$ret\n Data: ".print_r($data,true),1); //have already been paid so top up airtime
			//return;
		}
		if($amount < 5)
                {
                   $this->send_sms($sender_phone,"The minimum amount for airtime purchase is KShs 5 . Quick Airtime Solns 0711684717 ");
		   return '{ "status" : "01", "description" : "Accepted"}';
		   exit();


		}

		$rep = $this->check_last_purchase($sender_phone,$amount,$transaction_reference);
		//var_dump($rep);
		if($rep !== false) //there was a purchase made within last 5 minutes // insert into pending purchases table
		{			
		    $this->send_sms($sender_phone,$rep);	
		 	return;
		}
		//exit();
		//$data[''] = ;
		//error_log("<pre>$postdata</pre>");
		//$sql = "insert into mpesa_details values('','$business_number','$amount','$transaction_reference','$internal_transaction_id','$transaction_timestamp','$sender_phone','$first_name','$middle_name','$last_name',now())";
		//error_log($sender_phone);
		//if($sender_phone == "+254711684717")
		{
		        
			if($this->sambaza($sender_phone,$amount,$transaction_reference) === false)
			{
			  error_log("Sambaza failed ");
			  $this->buy_airtime($sender_phone,$amount,$transaction_reference,1);//src is 1
			  $method = "AT";
			}
			else
			{
                          $method = "Sambaza";
			}
			if($det === false)
			{
			   error_log("Reply to kopokopo");
			   return '{ "status" : "01", "description" : "Accepted"}';
			}
			return $method;
			exit();
		}
		//$this->buy_airtime($sender_phone,$amount,$transaction_reference,1);//src is 1
		//return '{ "status" : "01", "description" : "Accepted"} ';
		


	}	
	function first_time($num,$fname)
	{
	   error_log("checking $num");
	   if($this->airtimemodel->check_first_time($num))
	   {
	      $msg = "Welcome $fname to Quick Airtime Solns, for all your Safaricom airtime needs. Our Till No is 5089673. Inquiries 0711 684 717";
	      error_log($msg);
	      $this->send_sms($num,$msg);


	   }

	}

	function new_num($num,$chan)
	{
		$l = db();
		$sql = "insert into numbers set channel='$chan',num='$num'";
		if(mysqli_query($l,$sql))
		{
			return 1;
		}
		else
		{
			return 2;
		}	
	}

	function update_topup()
	{
		extract($_POST);
		//$data = file_get_contents("php://input");
		//return $this->input->post();

                $this->airtimemodel->update_topup($this->input->post('requestId'),$this->input->post('status'));
		if($this->input->post('status') != "Success")
		{
                   $this->send_sms("+254715532259","Urgent: **Failed** status during top up");
		}

	}

	function del_num($id)
	{
		$l = db();
		$sql = "delete from numbers where id='$id'";
		if(mysqli_query($l,$sql))
		{
			return 1;
		}
		else
		{
			return 2;
		}	
		
		
	}
	function get_at_balance()
	{
			require_once "AfricasTalkingGateway.php";

			//Specify your credentials
			$username = "xclucv";
			$apiKey   = "b6401e4fbff39ed62e68983fe1051530334dee743c9d693630079e4018ba9383";


			//Create an instance of our awesome gateway class and pass your credentials
			$gateway = new AfricasTalkingGateway($username, $apiKey);

			$results = $gateway->getUserData();
                 //        var_dump($results);
			 return $results->balance;


	}
	function buy_airtime($phone,$amount,$mpesa_ref,$src)
	{

			require_once "AfricasTalkingGateway.php";

			//Specify your credentials
			$username = "xclucv";
			$apiKey   = "b6401e4fbff39ed62e68983fe1051530334dee743c9d693630079e4018ba9383";

			//Specify the phone number/s and amount in the format shown
			//Example shown assumes we want to send KES 100 to two numbers
			// Please ensure you include the country code for phone numbers (+254 for Kenya in this case)
			// Please ensure you include the country code for phone numbers (KES for Kenya in this case)
			//error_log("phone is $phone");
			$nums = array($phone);
			$c = count($nums);
			$i = 0;
			$recipients = array();
			
			$amt = "KES $amount";
			while($i < $c)
			{
				// $phone = "+254".substr($nums[$i],1,strlen($nums[$i])); check if it comes as 07...
				$recipients[] = array("phoneNumber"=>$phone, "amount"=>$amt);
				
				$i++;
			}
			/*($recipients = array(
				array("phoneNumber"=>"+254711XXXYYY", "amount"=>"KES 100"),
				array("phoneNumber"=>"+254733YYYZZZ", "amount"=>"KES 100")
			);*/

			//Convert the recipient array into a string. The json string produced will have the format:
			// [{"amount":"KES 100", "phoneNumber":"+254711XXXYYY"},{"amount":"KES 100", "phoneNumber":"+254733YYYZZZ"}]
			//A json string with the shown format may be created directly and skip the above steps
			$recipientStringFormat = json_encode($recipients);

			//Create an instance of our awesome gateway class and pass your credentials
			$gateway = new AfricasTalkingGateway($username, $apiKey);

			/*************************************************************************************
			 NOTE: If connecting to the sandbox:

			 1. Use "sandbox" as the username
			 2. Use the apiKey generated from your sandbox application
				https://account.africastalking.com/apps/sandbox/settings/key
			 3. Add the "sandbox" flag to the constructor

			 $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
			**************************************************************************************/

			// Thats it, hit send and we'll take care of the rest. Any errors will
			// be captured in the Exception class as shown below
			$resp = array();
			try {
			$requestOptions = array();
			$options['idempotencyKey'] = $mpesa_ref;
			$requestOptions['headers'] = [
                'Idempotency-Key' => $mpesa_ref,
            ];
			$results = $gateway->sendAirtime($recipientStringFormat,$mpesa_ref);
            //var_dump($results);
			foreach($results as $result) {
			/*	echo $result->status;
				echo $result->amount;
				echo $result->phoneNumber;
				echo $result->discount;
				*/
				//$l = db();
				$data = array();
				$data['status'] = $result->status;
				$data['amount'] = $result->amount;
				$data['phone'] = $result->phoneNumber;
				$data['mpesa_ref'] = $mpesa_ref;
				$data['discount'] = $result->discount;
				$data['request_id'] = $result->requestId;
				$data['error_msg'] = $result->errorMessage;
				//$data['date_updated'] = ;
				$data['final_status'] = "";
				//$data['topup_method'] = "AT";
				
				/* echo $sql = "insert into topups values('','".$result->status."','".$result->amount."','".$result->phoneNumber."','".$result->discount."',
				'".$result->requestId."','".$result->errorMessage."',now(),NULL,'')";
				mysqli_query($l,$sql);
				echo mysqli_error($l); */
				
				//Error message is important when the status is not Success
				//echo $result->errorMessage;
				$resp[] = $result;
				}
			}
			catch(AfricasTalkingGatewayException $e){
				//echo $e->getMessage();
				$resp[] = $e->getMessage();
			}
			/*
			array(1) { [0]=> object(stdClass)#19 (6) { ["amount"]=> string(12) "KES 200.0000" ["discount"]=> string(1) "0" ["errorMessage"]=> string(19) "Insufficient Credit" ["phoneNumber"]=> string(13) "+254715532259" ["requestId"]=> string(4) "None" ["status"]=> string(6) "Failed" } } insert into topups values('','Failed','KES 200.0000','+254715532259','0', 'None','Insufficient Credit',now(),NULL,'') 
			
			array(1) { [0]=> object(stdClass)#19 (6) { ["amount"]=> string(11) "KES 10.0000" ["discount"]=> string(10) "KES 0.4000" ["errorMessage"]=> string(4) "None" ["phoneNumber"]=> string(13) "+254715532259" ["requestId"]=> string(38) "ATQid_e54a0d375632ec38e10bed64df09dc97" ["status"]=> string(4) "Sent" } } insert into topups values('','Sent','KES 10.0000','+254715532259','KES 0.4000', 'ATQid_e54a0d375632ec38e10bed64df09dc97','None',now(),NULL,'') 
			
			
			*/
			
			if($result->status == "Failed" && $result->errorMessage == "Insufficient Credit")
			{
				//echo "no funsd";
				$this->no_funds($phone,$amount,$mpesa_ref,$src);
				return;
			}
			if($result->status == "Sent" && $result->errorMessage == "None") //success
			{
				//$this->no_funds($amount,$phone);
			        error_log(print_r($data,true));	
				$this->airtimemodel->new_topup($data);
			//	$this->airtimemodel->new_purchase($phone);
				return;
			}
			return $resp;

	}
	//Done
    function check_last_purchase($num,$amount,$mpesa_ref)
	{
		//check if another purchase done in last five minutes. Return true/false
		//check if in purchase table, if there gen message saying when airtime will be sent else return false
		$resp = $this->airtimemodel->check_last_purchase($num,$amount,$mpesa_ref);
		return $resp;
	}

	function check_first_purchase($num)
	{
        //check if its the first time the number is making the purchase. If first time, send sms with till no and every 5 minutes rule
	}
	
	function ping()
	{
           $output = shell_exec('/usr/bin/ps -C php -f');
	   //var_dump($output);
	   if (strpos($output, "/usr/bin/php /var/www/html/airtime/goip/Server.php")===false) { 
            shell_exec('sudo usr/bin/php /var/www/html/airtime/goip/Server.php > /dev/null 2>&1 &');
	   }
	   else
	   {
	     //echo "Script running";
	   }
		//check if service is running
	}
	function update_last_purchase_info()
	{
		//run as a cron job. Check the purchases table and remove any thats lasted more than 6 minutes
		$this->airtimemodel->update_last_purchase_info();
	}
	function check_min()
	{
		//check airtime amount greater than 10 bob. Send sms if otherwise. Put minimum amount in advert
	}
	function update_payment()//process response from z
	{
		//update payments table with AT reference
	}
	
	function no_funds($phone,$amount,$mpesa_ref,$src)//src 1 means first time so send msg to client, 2 means being repeated so dont send msg to client
	{
		$timestamp = time() - 3;
		$this->airtimemodel->add_pending_purchase($phone,$timestamp,$amount,$mpesa_ref);
             $msg = "Urgent: No money in AT account for purchase of $amount. Top Up";
		$this->send_sms("+254715532259",$msg); 
		if($src == 1)
		{

                 $msg_client = "Quick Airtime: Your airtime request of $amount has been received and is being processed.";	
		$this->send_sms($phone,$msg_client);
		}
	}
	
	function pending()
	{
		//run as cron. fetch from pending purchases and run then delete from that table 
	    $res = $this->airtimemodel->fetch_pending();
		//var_dump($res);	
		$c = count($res);
	        if($c > 0)
		{
	 	   error_log("Pending cron running");
		}
		$i = 0;
		while($i < $c)
		{
			$d = $res[$i];
			$this->airtimemodel->add_history_pending_purchase($d['id'],$d['phone'],$d['purchase_time'],$d['amount'],$d['mpesa_ref']);
			$this->airtimemodel->delete_pending($d['id']);
			//$this->buy_airtime($d['phone'],$d['amount'],$d['mpesa_ref'],2);//src = 2 being repeated
			if($this->sambaza($d['phone'],$d['amount'],$d['mpesa_ref']) === false)
			{
			  error_log("Sambaza failed ");
			  $this->buy_airtime($d['phone'],$d['amount'],$d['mpesa_ref'],2);//src = 2 being repeated
			}
			
			
			
			$i++;
		}
		
	}
	function delete_from_pending()
	{
		//delete from table purchases after 5 minutes have passed. Run as a cron
		
	}
	
	//1: New payment workflow: payment received->check last 5 min->insert into airtime top ups table with mpesa ref->check if new number
	//2: Airtime update workflow: update top ups table->if success->check first num->if first num send msg with welcome details else done
	//                                                ->if error->send message saying: Airtime will be sent, raise error message
	//Insufficient funds->Insert into pending purchases,send sms to customer saying being processed, send to Admin to say no money, insert into pending purchases table
	
	//Test cases:
	//->Test when the funds are insufficient then topped up then cron processes the airtime 
	//->Test the 5 minute interval
	function reps($id = null)
	{
	        $res['bal'] =  $this->get_at_balance();
		$res['from'] = $this->input->post('from');	
		$res['to'] = $this->input->post('to');
		$res['id'] = $id;
		$this->load->view('airtime',$res);
		
	}
	function graphs()
	{
	     $res = array();
	     $res['data'] = $this->airtimemodel->get_graph_data();
             $this->load->view('graphs',$res);

	}

	function sambaza($phone,$amount,$mpesa_ref)
	{
	   /*$r = $this->reconcile(23,1);
	   //var_dump($r);
	   exit();*/
	  if($this->check_saf_number($phone) === false)
	  {
	    return false;

	  }
	  $line = $this->get_available_line($amount);
	  //var_dump($line);
	  if($line === false)//no line available so sambaza impossible therefore use AT
	  {
			  error_log("insidwe 312");
            return false;

	  }
	  $bal = $this->airtimemodel->get_balance(1);
	  $phone = "0".substr($phone,-9);
	  $d = date("F j, Y, g:i a");
	  $amount = intval($amount);
	  $tot = $bal + 20;
	  if($phone == "0711684717")
	  {
	    $bal = 100000;
	  }
	  if($bal < $amount)
	  {
            $this->send_sms("+254715532259","Insufficient balance to sambaza $amount, Balance is $bal $d" );
			  error_log("inside 12");
	    return false;

	  }
	  if($bal - $amount < 20)//20 must remain
	  {
            $this->send_sms("+254715532259","Insufficient balance to sambaza $amount, No sambaza Balance is $bal $d" );
			  error_log("inside 12");
	    return false;

	  }
          error_log("new sambaza tx");//  $this->send_sms("+254715532259","Insu sambaza $amount, Balance is $bal" );
          $id = $this->airtimemodel->new_sambaza($line,$phone,$amount,$mpesa_ref);
	  //format phone to 07 from +254
	  //$phone = "0".substr($phone,3,strlen($phone));

	  $msg = "$amount#$phone";
	  //$msg = "bal";
	  $resp = $this->goip_send($msg,$line,"140");
          $this->airtimemodel->update_sambaza($id,$resp);	
	  //check resp for true or false
	  //var_dump($resp);
	  flush();
	  if($resp === 0)
	  {
	    $this->airtimemodel->update_failed_sambaza($id,$bal);
	    return false;


	  }
	  
	  if($resp === 200)//sent to goip successfully so poll for response
	  {
	    $rep = false;
	    $n = 1;
	    while(!$rep)
	    {
              $rep = $this->check_goip_resp($id);
	      error_log(var_dump($rep));
	      
	      sleep(2);
	      $n++;
	      if($n == 18)//took 36 seconds avg is 21 secs
	      {
                //send sms about being processed
		$phone = "+254".substr($phone,-9);
		$msg = "Your airtime request of $amount has been received and is being processed.Support 0711684717" ;
		$this->send_sms($phone,$msg);
		error_log("36 seconds gone");

	      }
              flush();
	      error_log("n is $n \n");
	      if($n == 30)//took 60 seconds
	      {
                //send sms about being processed
		//set line status to not being used
		//check balance via sms
		//compare with bal after for previous
		//if bal reduced by $amount it was successful so break and return success
		//if bal didnt change didnt succeed so notify admin and top up with AT
		error_log("wtf 40 seconds gone");
		$this->send_sms("+254715532259","Goip topup response taking 60 secs. Curl resp 200 id: $id, line:$line, amount: $amount ");
		$r = $this->reconcile($id,$line);
		//var_dump($r);
		$rep = true;
		flush();
		break;

	      }
	    

	    }
	    if($rep === true)//everything went well hurray
	    {
	       //echo "Sambaza went well";
               return true;
	    }
	    flush();
	    return $rep;

	  }
	  //something didnt work,should get here if curl didnt return 200
	  return false;
	}
	function check_goip()
	{//check if goip is up //curl will return non 200 response so maybe not needed


	}
	function get_available_line($amount)
	{
	   //get line with the balance,status is logged in, recently updated status, last top up was a success and happened $x minutes ago
	   //use also round robin method
	   //get list of available lines
	   //full implementation later
	   //check balance if cant be used return false
	   //check if GOIP is online and sim card is active
	   
	   return $this->airtimemodel->line_available(1);

	}
	function check_goip_resp($id)
	{
	   $rep = $this->airtimemodel->check_goip_resp($id);
	   return $rep;

	}
	function goip_send($msg,$line,$to)
	{
	        $rand = rand();
		$url = 'http://10.0.10.2/default/en_US/sms_info.html';
		// $line sim card to use in my case #1
		$telnum = $to; // phone number to send sms
		$smscontent = $msg; //your message
		$username = "admin"; //goip username
		$password = "admin"; //goip password

		$fields = array(
		'line' => urlencode($line),
		'smskey' => urlencode($rand),
		'action' => urlencode('sms'),
		'telnum' => urlencode($telnum),
		'smscontent' => urlencode($smscontent),
		'send' => urlencode('send')
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
		curl_setopt($ch, CURLOPT_PORT, 80);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
 		curl_setopt($ch, CURLOPT_VERBOSE, true);
 		$verbose = fopen('php://temp', 'w+');
        	curl_setopt($ch, CURLOPT_STDERR, $verbose);

		//execute post
		$rep = curl_exec($ch);
 
                rewind($verbose);
                $verboseLog = stream_get_contents($verbose);
                //echo "Verbose information:<br/><pre>" . htmlspecialchars($verboseLog) .  "</pre><br/> ";
		flush();
		//var_dump($rep);
		//echo "<br>";
		$httpcode = curl_getinfo($ch/*);var_dump( $httpcode);//*/, CURLINFO_HTTP_CODE);
		return $httpcode;

	}
	function reconcile($id,$line)
	{
          //check that samabaza was done its the response that never arrived
	  $msg = "bal";
	  $to = "144";
	  $httpresp = $this->goip_send($msg,$line,$to);
	  if($httpresp != 200)//cant reach goip, send sms
	  {
	    
	    $this->send_sms("+254715532259","Curl error code $httpresp when doing recon. Failed tx $id on line $line");
	    $this->airtimemodel->new_recon_tx($id,$line,"GOIP SEND FAIL");//new_recon($id,$line);
            return 200;

	  }
	  $this->airtimemodel->new_recon_tx($id,$line,"SENT TO GOIP");//new_recon($id,$line);
	  return 100;
	 
	}
	function check_saf_number($phone)
	{
	  $prefix = intval(substr($phone,-9,3));
	  $s1 = range(700,729,1);
	  //print_r($s1);
	  $s2 = range(740,749,1);
	  //print_r($s2);
	  array_push($s2,746,748);
	  $s3 = range(790,799,1);
	  $s4 = range(110,115,1);
	  $s5 = range(768,769,1);
	  $s6 = range(757,759,1);
	  //print_r($s4);

	  $prefixes = array_merge($s1,$s2,$s3,$s4);

	  if(in_array($prefix,$prefixes))
	  {
	    return true;
	  }
	  else
	  {
	    return false;

	  }

	}
}	

?>

